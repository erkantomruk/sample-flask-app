from flask import Flask
from flask_dance.contrib.github import make_github_blueprint
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
app.secret_key = "secret"
ma = Marshmallow(app)


blueprint = make_github_blueprint(
    client_id="your_client_id",
    client_secret="your_client_secret",
)
app.register_blueprint(blueprint, url_prefix="/login")

from user.view import *

if __name__ == '__main__':
    app.run(ssl_context='adhoc')
