from flask import jsonify, request, json, url_for, redirect
from flask_dance.contrib.github import github

from myapp import app, db
from user.model import User, UserSchema


@app.route('/users')
def get_users():
    if not github.authorized:
        return redirect(url_for("github.login"))
    account_info = github.get("/user")
    if account_info.ok:

        all_users = User.query.all()
        users_schema = UserSchema(many=True)

        return jsonify(users_schema.dump(all_users).data)


@app.route('/users/<name>')
def get_user_by_name(name):
    user = User.query.filter(User.name == name).first()
    print(user.name)
    user_schema = UserSchema()
    return jsonify(user_schema.dump(user).data)


@app.route('/update-email/<name>', methods=['PUT'])
def update_email_by_name(name):
    try:
        user = User.query.filter(User.name == name).first()
        user.email = 'newemail'
        db.session.commit()
        return '', 200
    except:
        return '', 404


@app.route('/delete-user', methods=['DELETE'])
def delete_user_by_name():
    try:
        param = request.args.get('name')
        user = User.query.filter(User.name == param).first()
        user.email = 'newemail'
        db.session.delete(user)
        db.session.commit()
        return '', 200
    except:
        return '', 404


@app.route('/add-user', methods=['POST'])
def add_user():
    data = request.data
    data_dict = json.loads(data)
    db.session.add(User(data_dict['name'], data_dict['email']))
    db.session.commit()
    return '', 200
